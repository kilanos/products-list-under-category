<?php

/* Show child categories */

if (VmConfig::get('showCategory', 1) and empty($this->keyword)) {
	if ($this->category->haschildren) {

		// Category and Columns Counter
		$iCol = 1;
		$iCategory = 1;

		// Calculating Categories Per Row
		$categories_per_row = VmConfig::get('categories_per_row', 3);
		$category_cellwidth = ' width' . floor(100 / $categories_per_row);

		// Separator
		$verticalseparator = " vertical-separator";
		?>

		<div class="category-view">
			<div class="layout-public">
				<h1><?php echo $this->category->category_name; ?> Categories </h1>
				<?php // Start the Output
				if (!empty($this->category->children)) {
					foreach ($this->category->children as $category) {

						// Show the horizontal seperator
						if ($iCol == 1 && $iCategory > $categories_per_row) {
							?>
							<div class="horizontal-separator"></div>
						<?php
						}

						// this is an indicator wether a row needs to be opened or not
						if ($iCol == 1) {
							?>
							<div class="row">
						<?php
						}

						// Show the vertical seperator
						if ($iCategory == $categories_per_row or $iCategory % $categories_per_row == 0) {
							$show_vertical_separator = ' ';
						} else {
							$show_vertical_separator = $verticalseparator;
						}

						// Category Link
						$caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $category->virtuemart_category_id);

						// Show Category
						?>
						<div class="category">


							<h2>
								<a href="<?php echo $caturl ?>"
								   title="<?php echo $category->category_name ?>">

									<?php // if ($category->ids) {
									echo $category->images[0]->displayMediaThumb("", FALSE);
									//}
									?>
									<br/>
									<?php echo $category->category_name ?>

								</a>
							</h2>

						</div>
						<?php
						$iCategory++;

						// Do we need to close the current row now?
						if ($iCol == $categories_per_row) {
							?>
							<div class="clear"></div>
							</div>
							<?php
							$iCol = 1;
						} else {
							$iCol++;
						}
					}
				}
				// Do we need a final closing row tag?
				if ($iCol != 1) {
				?>
				<div class="clear"></div>
			</div>
			<?php } ?>
		</div>
		</div>

	<?php } ?>
<?php } ?>


<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 6556 2012-10-17 18:15:30Z kkmediaproduction $
 */

//vmdebug('$this->category',$this->category);
vmdebug('$this->category ' . $this->category->category_name);
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHTML::_('behavior.modal');
/* javascript for list Slide
  Only here for the order list
  can be changed by the template maker
*/
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";

$document = JFactory::getDocument();
$document->addScriptDeclaration($js);

/*$edit_link = '';
if(!class_exists('Permissions')) require(JPATH_VM_ADMINISTRATOR.DS.'helpers'.DS.'permissions.php');
if (Permissions::getInstance()->check("admin,storeadmin")) {
	$edit_link = '<a href="'.JURI::root().'index.php?option=com_virtuemart&tmpl=component&view=category&task=edit&virtuemart_category_id='.$this->category->virtuemart_category_id.'">
		'.JHTML::_('image', 'images/M_images/edit.png', JText::_('COM_VIRTUEMART_PRODUCT_FORM_EDIT_PRODUCT'), array('width' => 16, 'height' => 16, 'border' => 0)).'</a>';
}

echo $edit_link; */
if (empty($this->keyword)) {
	?>
	<div class="layout-public">
		<div class="product-categories">

			<?php if ($this->category->haschildren) { ?>
				<!--has childen-->
			<?php } else { ?>
				<h1><?php echo $this->category->category_name; ?></h1>
			<?php } ?>

			<?php echo $this->category->category_description; ?>
		</div>
	</div>
<?php } ?>


<?php if ($this->category->haschildren) { ?>
	<!--has childen-->
<?php } else { ?>


	<div class="layout-public">
	<div class="browse-view">
	<?php
	if (!empty($this->keyword)) {
		?>
		<h3><?php echo $this->keyword; ?></h3>
	<?php
	} ?>
	<?php if ($this->search !== NULL) { ?>
		<form action="<?php echo JRoute::_('index.php?option=com_virtuemart&view=category&limitstart=0&virtuemart_category_id=' . $this->category->virtuemart_category_id); ?>"
			method="get">

			<!--BEGIN Search Box -->
			<div class="virtuemart_search">
				<?php echo $this->searchcustom ?>
				<br/>
				<?php echo $this->searchcustomvalues ?>
				<input name="keyword" class="inputbox" type="text" size="20"
					 value="<?php echo $this->keyword ?>"/>
				<input type="submit" value="<?php echo JText::_('COM_VIRTUEMART_SEARCH') ?>" class="button"
					 onclick="this.form.keyword.focus();"/>
			</div>
			<input type="hidden" name="search" value="true"/>
			<input type="hidden" name="view" value="category"/>

		</form>
		<!-- End Search Box -->
	<?php } ?>

	<?php // Show child categories
	if (!empty($this->products)) {
		?>
		<div class="orderby-displaynumber">			
			<?php echo $this->orderByList['orderby']; ?> <?php echo $this->orderByList['manufacturer']; ?>
		</div> <!-- end of orderby-displaynumber -->



		<?php
		// Category and Columns Counter
		$iBrowseCol = 1;
		$iBrowseProduct = 1;

		// Calculating Products Per Row
		$BrowseProducts_per_row = $this->perRow;
		$Browsecellwidth = ' width' . floor(100 / $BrowseProducts_per_row);

		// Separator
		$verticalseparator = " vertical-separator";

		// Count products ?? why not just count ($this->products)  ?? note by Max Milbers
		$BrowseTotalProducts = 0;
		foreach ($this->products as $product) {
			$BrowseTotalProducts++;
		}
		
		//sorting the products
		$this->products = sort_arr_of_obj($this->products,'product_override_price','asc');
		
		// Start the Output
		foreach ($this->products as $product) {					
			

			// Show the horizontal seperator
			if ($iBrowseCol == 1 && $iBrowseProduct > $BrowseProducts_per_row) {
				?>
				<div class="horizontal-separator"></div>
			<?php
			}

			// this is an indicator wether a row needs to be opened or not
			if ($iBrowseCol == 1) {
				?>
				<div class="row">
			<?php
			}

			// Show the vertical seperator
			if ($iBrowseProduct == $BrowseProducts_per_row or $iBrowseProduct % $BrowseProducts_per_row == 0) {
				$show_vertical_separator = ' ';
			} else {
				$show_vertical_separator = $verticalseparator;
			}

			// Show Products
			?>
			<div class="product floatleft<?php echo $Browsecellwidth . $show_vertical_separator ?>">
				<div class="spacer">
					<div class="center">


						<?php $prodID = $product->virtuemart_product_id; ?>

						<?php			
						
						//display the override price to check
						echo $product->product_override_price;
						
						$db = JFactory::getDbo();
						$query = "SELECT virtuemart_product_id, virtuemart_customfield_id, virtuemart_custom_id, custom_value FROM  `rkop_virtuemart_product_customfields` WHERE `virtuemart_product_id` = $prodID";


						$db->setQuery($query);
						$db->query();
						$row = $db->loadRowList();
						//print_r($row);
						?>



						<div class="flag">
							<?php
							if ($row['1']['3']) {
								$new = 1;
								echo '<div class="new">New</div>';
							};

							if ($row['2']['3']) {
								$sale = 1;
								echo '<div class="sale">Sale</div>';
							}; ?>
						</div>


						<a class="thumb" title="<?php echo $product->link ?>" rel="vm-additional-images"
						   href="<?php echo $product->link; ?>">


							<?php
							if ($this->show_prices == '1') {
								if ($product->prices['salesPrice'] <= 0 and VmConfig::get('askprice', 1) and  !$product->images[0]->file_is_downloadable) {
									echo JText::_('COM_VIRTUEMART_PRODUCT_ASKPRICE');
								}
								//todo add config settings
								if ($this->showBasePrice) {
									echo $this->currency->createPriceDiv('basePrice', 'COM_VIRTUEMART_PRODUCT_BASEPRICE', $product->prices);
									echo $this->currency->createPriceDiv('basePriceVariant', 'COM_VIRTUEMART_PRODUCT_BASEPRICE_VARIANT', $product->prices);
								}
								echo $this->currency->createPriceDiv('variantModification', 'COM_VIRTUEMART_PRODUCT_VARIANT_MOD', $product->prices);

								if (round($product->prices['salesPriceWithDiscount'], $this->currency->_priceConfig['salesPrice'][1]) != $product->prices['salesPrice']) {
									echo $this->currency->createPriceDiv('salesPriceWithDiscount', 'COM_VIRTUEMART_PRODUCT_SALESPRICE_WITH_DISCOUNT', $product->prices);
								}
								echo $this->currency->createPriceDiv('salesPrice', 'COM_VIRTUEMART_PRODUCT_SALESPRICE', $product->prices);
							} ?>

							<?php
							echo $product->images[0]->displayMediaThumb('class="browseProductImage"', false);
							?>
						</a>

						<!-- The "Average Customer Rating" Part -->
						<?php if ($this->showRating) { ?>
							<span class="contentpagetitle"><?php echo JText::_('COM_VIRTUEMART_CUSTOMER_RATING') ?>
								:</span>
							<br/>
							<?php
							// $img_url = JURI::root().VmConfig::get('assets_general_path').'/reviews/'.$product->votes->rating.'.gif';
							// echo JHTML::image($img_url, $product->votes->rating.' '.JText::_('COM_VIRTUEMART_REVIEW_STARS'));
							// echo JText::_('COM_VIRTUEMART_TOTAL_VOTES').": ". $product->votes->allvotes;
							?>
						<?php } ?>
						<?php
						if (VmConfig::get('display_stock', 1)) {
							?>
							<!-- 						if (!VmConfig::get('use_as_catalog') and !(VmConfig::get('stockhandle','none')=='none')){?> -->
							<div class="paddingtop8">
								<span class="vmicon vm2-<?php echo $product->stock->stock_level ?>"
									title="<?php echo $product->stock->stock_tip ?>"></span>
								<span class="stock-level"><?php echo JText::_('COM_VIRTUEMART_STOCK_LEVEL_DISPLAY_TITLE_TIP') ?></span>
							</div>
						<?php } ?>
					</div>

					<div class="productDetails">

						<h2><?php echo JHTML::link($product->link, $product->product_name); ?></h2>
						<p class="product_s_desc">
						<?php // Product Short Description
						if (!empty($product->product_s_desc)) {
							?>

								<?php echo shopFunctionsF::limitStringByWord($product->product_s_desc, 40, '...') ?>

						<?php } ?>
						</p>



						<?php
						if ($row['24']['3']) {
							$product_show_price = 1;
							$user =& JFactory::getUser();
							if ($user->get('guest') == 1 || $user->usertype == 'Registered') {
								?>


								<div class="product-price" id="productPrice<?php echo $product->virtuemart_product_id ?>">
								&nbsp;	<!-- DONT SHOW PRICE --></div>


								<p>
									<?php // Product Details Button
									echo JHTML::link($product->link, JText::_('COM_VIRTUEMART_ADDTOQUOTE'), array('title' => $product->product_name, 'class' => 'product-details'));
									?>
								</p>
							<?php } else { ?>

								<?php include 'customPriceBlock.php'; ?>
								<p>
									<?php // Product Details Button
									echo JHTML::link($product->link, JText::_('COM_VIRTUEMART_PRODUCT_DETAILS'), array('title' => $product->product_name, 'class' => 'product-details'));
									?>
								</p>
							<?php
							}
						} else {
							?>

							<?php include 'customPriceBlock.php'; ?>
							<p>
								<?php // Product Details Button
								echo JHTML::link($product->link, JText::_('COM_VIRTUEMART_PRODUCT_DETAILS'), array('title' => $product->product_name, 'class' => 'product-details'));
								?>
							</p>
						<?php } ?>






					</div>
					<div class="clear"></div>
				</div>
				<!-- end of spacer -->
			</div> <!-- end of product -->
			<?php

			// Do we need to close the current row now?
			if ($iBrowseCol == $BrowseProducts_per_row || $iBrowseProduct == $BrowseTotalProducts) {
				?>
				<div class="clear"></div>
				</div> <!-- end of row -->
				<?php
				$iBrowseCol = 1;
			} else {
				$iBrowseCol++;
			}

			$iBrowseProduct++;
		} // end of foreach ( $this->products as $product )
		// Do we need a final closing row tag?
		if ($iBrowseCol != 1) {
			?>
			<div class="clear"></div>

		<?php
		}
		?>

		<div class="pagination">
			<div class="width30 floatright display-number"><?php echo $this->vmPagination->getResultsCounter(); ?>
				<br/><?php echo $this->vmPagination->getLimitBox(); ?></div>
			<div class="vm-pagination">
				<?php echo $this->vmPagination->getPagesLinks(); ?>

			</div>

			<div class="clear"></div>
		</div> <!-- end of orderby-displaynumber -->

	<?php
	} elseif ($this->search !== NULL) {
		echo JText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
	}
	?>
	</div>
	<!-- end browse-view -->
	</div>


<?php 
} 

//sorting function on objective type
function sort_arr_of_obj($array, $sortby, $direction='asc') {
    
    $sortedArr = array();
    $tmp_Array = array();
    
    foreach($array as $k => $v) {
        $tmp_Array[] = strtolower($v->$sortby);
    }
    
    if($direction=='asc'){
        asort($tmp_Array);
    }else{
        arsort($tmp_Array);
    }
    
    foreach($tmp_Array as $k=>$tmp){
        $sortedArr[] = $array[$k];
    }
    
    return $sortedArr;
 
}

?>